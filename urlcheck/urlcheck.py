#!/usr/bin/python

# Copyright 2001-2003 James A. Treacy
# This code may be distributed under the terms of the GPL

import sys, urllib, sgmllib, htmllib, httplib, ftplib, formatter, urlparse, socket;
import re, signal, getopt;

TIMEOUT = 15
debug=0

def handler(signum, frame):
	# print 'Signal handler called with signal', signum
	raise IOError, "gave up on site (" + repr(TIMEOUT) + " second limit)"

def do_page(page_url):
	print "Looking into", page_url
	try:
		signal.signal(signal.SIGALRM, handler)
		signal.alarm(TIMEOUT)
		current = urllib.urlopen(page_url)
		signal.alarm(0)
		# print current.geturl()
		# print current.read()
	except (IOError, EOFError), arg:
		print "Error accessing page:", arg.args[0]
		return
	parse = htmllib.HTMLParser(formatter.NullFormatter())
	try:
		parse.feed(current.read())
		parse.close()
	except (AttributeError, IOError, TypeError, ValueError, sgmllib.SGMLParseError):
		print "  Error reading page:", page_url
	#if debug:
	#	print parse.anchorlist
	for url in parse.anchorlist:
		parts = urlparse.urlparse(url)
		parts = parts[0], parts[1], parts[2], parts[3], parts[4], ''
		urlnew = urlparse.urlunparse(parts)
		# print "TESTING:", url, urlnew
		url = urlparse.urljoin(page_url, urlnew)
		# print url
		check_url(url)
		httpdone[url] = 1

def check_url(url):
	# check url exist
	parts = urlparse.urlparse(url)
	if not parts[0]:
		print " ", url, " : Error: invalid url"
		return
	# if re.search('//', parts[2]):
	# 	print " ", url, "Error: too many '/'"
	path_tmp = re.sub('//+', '/', parts[2])
	query_tmp = re.sub('&amp;','&',parts[4])
	parts = parts[0], parts[1], path_tmp, parts[3], query_tmp, ''
	urlnew = urlparse.urlunparse(parts)
	if debug:
		if urlnew != url:
			print "DEBUG: ", url, "converted to", urlnew
	url = urlnew
	if urlseen.has_key(url):
		if re.search('\(200\)', urlseen[url]) or re.search('\(302\)', urlseen[url]):
			# print "  found a good url so not printing it"
			pass
		else:
			print "  " + url + urlseen[url]
			sys.stdout.flush()
		return
	else:
		msg = "If you see this, then msg didn't get set properly"
		if parts[0] == 'ftp':
			msg = check_ftp(url, parts)
		elif parts[0] == 'https':
			msg = " : can't check https for validity"
		elif parts[0] == 'mailto':
			msg = " : can't check mailto for validity"
		elif parts[0] == 'news':
			msg = " : can't check news for validity"
		elif parts[0] == 'file':
			try:
				current = urllib.urlopen(url)
				httplist.append(url)
				msg = " : is ok"
			except IOError, arg:
				msg = " : Error: "+ arg.args[1]
		elif parts[0] == 'http':
			msg = check_http(url, parts)
		else:
			msg = " : unknown url type"
		if re.search('\(200\)', msg) or re.search('\(302\)', msg):
			# print "  found a good url so not printing it"
			pass
		else:
			print "  " + url + msg
			sys.stdout.flush()
		urlseen[url] = msg

def check_http(url, parts):
	# must do http connection using httplib so can parse the return codes
	# only if the url is good should it be added to httplist
	# print "entering check_http with url =", url
	try:
		# print "host =", parts[1]
		# print "file =", parts[2]
		# print "url =", url
		# print "parts =", parts
		msg = ""
		if parts[1] in noncompliant:
			return " : Error: site uses a non-compliant server. Not checking"
		signal.signal(signal.SIGALRM, handler)
		signal.alarm(TIMEOUT)
		h = httplib.HTTP(parts[1])
		h.putrequest('HEAD', parts[2])
		h.putheader('Host', parts[1])
		# h.putheader('Accept', '*/*')
		h.endheaders()
		errcode, errmsg, headers = h.getreply()
		signal.alarm(0)
		# print "    errcode =",errcode
		# print "    errmsg  =",errmsg
		# print "    headers =",headers
		if errcode == 200 or errcode == 302:
			msg = " : (" + str(errcode) + ") " + errmsg
			headers = str(headers)
			#print "    headers =", headers
			type = re.findall("Content-Type:\s*text/html.*\n", headers)
			if len(type) or errcode == 302:
				add_url(url)
			#else:
				#print "not adding " + url + ", not html\n"
		elif errcode == 301:
			headers = str(headers)
			#print "    headers =", headers
			result = re.findall("Location:\s*(.*)\n", headers)
			if len(result):
				if (result[0]):
					msg = " : Error = (" + str(errcode) + ") " + errmsg + ". New URL: " + result[0]
				else:
					msg = " : Error = (" + str(errcode) + ") " + errmsg
		elif errcode == 400 or errcode == 403 or errcode == 404:
			# print "TRYING AGAIN"
			signal.signal(signal.SIGALRM, handler)
			signal.alarm(TIMEOUT)
			h = httplib.HTTP(parts[1])
			h.putrequest('HEAD', url)
			h.putheader('Host', parts[1])
			h.endheaders()
			errcode, errmsg, headers = h.getreply()
			signal.alarm(0)
			# print "    headers =", headers
			if errcode == 200 or errcode == 302:
				add_url(url)
				msg = " : (" + str(errcode) + ") " + errmsg 
			else:
				msg = " : Error = (" + str(errcode) + ") " + errmsg 
		else:
			msg = " : Error = (" + str(errcode) + ") " + errmsg 
	except IOError, arg:
		msg = " : IOError: " + str(arg.args[0])
	except socket.error, arg:
		msg = " : Error: " + str(arg.args)
	except ValueError:
		msg = " : Error: URL not valid "
	return msg

def ftp_file_exists(string):
	kluge[0] = 1

def check_ftp(url, parts):
	try:
		signal.signal(signal.SIGALRM, handler)
		signal.alarm(TIMEOUT)
		ftp = ftplib.FTP(parts[1])
		ftp.login()
		# listcmd = "LIST " + parts[2]
		# print "    listcmd =", listcmd
		#ftp.retrlines(listcmd)
		kluge[0] = 0
		ftp.dir(parts[2], ftp_file_exists)
		if kluge[0]:
			msg = " : is ok"
		else:
			msg = " : Error: file doesn't exist"
		ftp.quit()
		signal.alarm(0)
	except socket.error, arg:
		msg = " : Error: " + str(arg.args)
	except (IOError, ftplib.error_perm, ftplib.error_temp, EOFError), arg:
		msg = " : Error: " + str(arg)
	#retrlines (command[, callback])
	#dir (argument[, ...])
	#cwd (pathname)
	return msg

def add_url(url):
	for x in require:
		if not re.search(x, url):
			# print "    ", url, "is missing", x
			return
	for x in ignore:
		if re.search(x, url):
			# print "    ", url, "includes", x, "which is being ignored"
			return
	httplist.append(url)
		

# BEGIN MAIN PROGRAM

kluge = []
kluge.append(0)

httplist = []
httpdone = {}
urlseen = {}
require = []
ignore = []
noncompliant = ['www.corel.com', 'www.sun.se', 'www.mapblast.com', 'www.borders.com', 'www.sun.no']

def append_from(path, list):
	file = open(path, "r");
	if file:
		for line in file.readlines():
			if line[-1] == '\n':
				line = line[:-1]
			print line
			require.append(line)
	else:
		print "Can't open " + path
		sys.exit(1)

options, args = getopt.getopt(sys.argv[1:], "", ["require=", "ignore=", "requirefrom=", "ignorefrom=", "non-compliant", "non-compliant-from="])
for option in options:
	if option[0] == '--require':
		require.append(option[1])
	elif option[0] == '--ignore':
		ignore.append(option[1])
	elif option[0] == '--non-compliant':
		noncompliant.append(option[1])
	elif option[0] == '--requirefrom':
		append_from(option[1], require)
	elif option[0] == '--ignorefrom':
		append_from(option[1], ignore)
	elif option[0] == '--non-compliant-from':
		append_from(option[1], noncompliant)
			
httplist = args

if httplist == []:
	print "Usage: urlcheck.py [OPTION]... URL...\n"
	print "\t--require STRING\tOnly check URLs containing STRING."
	print "\t--requirefrom FILE\tOnly check URLs containing strings in FILE."
	print "\t--ignore STRING\tDon't check URLs containing STRING."
	print "\t--ignorefrom FILE\tDon't check URLs containing strings in FILE."
	print "\t--non-compliant STRING\tDon't visit hosts containing STRING."
	print "\t--non-compliant-from FILE\tDon't visit hosts containing strings in FILE."
	sys.exit(1)
		
url = httplist.pop(0)
while (1):
	do_page(url)
	httpdone[url] = 1
	if (len(httplist) > 0):
		url = httplist.pop(0)
	else:
		break
print "Program Finished Normally"
